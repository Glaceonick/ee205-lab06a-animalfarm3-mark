
#include <iostream>
#include "list.hpp"

using namespace std;

namespace animalfarm {


	Node* head = new Node();

/*
	public void push_front(Node* newNode) {
		if (head == NULL) {
			head = new Node(newNode);
			return;
		}
		Node current = head;
		while (current.next != NULL) {
			current = current.next;
		}
		current.next = new Node(newNode);
	}
*/

	const bool SingleLinkedList::empty() const {
		if (head == nullptr) {
			return true;
		}
		return false;
	
	}

	
	void SingleLinkedList::push_front(Node* newNode) {
		newNode->next = head;
		head = newNode;
	}

/*
	public void deleteWithValue(int data) {
		if (head == NULL) return;
		if (head.data == data) {
			head = head.next;
			return;
		}

		Node current = head;
		while (current.next != NULL) {
			if (current.next.data == data) {
				current.next = current.next.next;
				return;
			}
			current = current.next;
		}
	}
*/

	Node* SingleLinkedList::pop_front() {
		if (head == NULL) {
			return nullptr;
		}
		
		head = head->next;
		return 0;

	}

	Node* SingleLinkedList::get_first() const {
		return head;
	}

	Node* SingleLinkedList::get_next(const Node* currentNode) const {
		/*
		Node* current;
		current = currentNode;
		current = current->next;
		*/
		return currentNode->next;
	}

	unsigned int SingleLinkedList::size() const {
		int i = 0;
		Node* current;
		current = head;
                while (current != NULL) {
                        current = current->next;
                	i++;
		}
		return i;

	}


}


