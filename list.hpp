
#pragma once
#include "node.hpp"


namespace animalfarm {

	class SingleLinkedList {
	public:
		const bool empty() const;
		void push_front( Node* newNode );
		Node* pop_front();
		Node* get_first() const;
		Node* get_next( const Node* currentNode ) const;
		unsigned int size() const;
	protected:
		Node* head = nullptr;

	};



}
